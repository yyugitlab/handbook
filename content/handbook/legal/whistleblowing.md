---
title: Whistleblowing at GitLab
description: "GitLab's Whistleblowing HB page, with links to whistleblowing polcies"
---

{{% alert title="This is a Controlled Document" color="danger" %}}
Inline with GitLab's regulatory obligations, changes to [controlled documents]({{< ref "controlled-document-procedure" >}}) must be approved or merged by a code owner. All contributions are welcome and encouraged.
{{% /alert %}}

Whistleblowing at GitLab
{.h1}

GitLab Inc. (collectively with its subsidiaries, “GitLab”, “we”, “our”) is committed to the highest standards of ethics, integrity, and accountability and to complying with applicable laws, rules, and regulations. You are required to perform your duties and responsibilities with honesty and integrity and to comply with all applicable laws, rules, and regulations, as well as GitLab’s policies. If you become aware of an actual or suspected violation of our policies or of any laws or regulations, it is your responsibility and obligation to report it and you should do so without fear of reprisal or retaliation of any kind.
GitLab has established whistleblower reporting channels and policies* to enable the reporting of any known or suspected improper activities that are unlawful or otherwise violate our policies so that we can investigate and resolve potential violations as quickly and efficiently as possible. You are encouraged to use the reporting channels provided, the details of which are set out in each policy and are also [set out in the Handbook](https://handbook.gitlab.com/handbook/people-group/#how-to-report-violations), to report all known or suspected improper activities, noting that each is designed to facilitate confidential, or in some cases anonymous, options for reporting any improper activities.
In addition to our [GitLab Inc. policy](https://drive.google.com/drive/folders/1kB3k5FRnR3OUBP0Eyo3SxxyPKeiRFfUk), additional policies for certain EU countries are accessible below. The GitLab Inc. policy will apply, unless a country-specific policy applies to an individual (because they are employed by the GitLab entity in that country). If the protections are greater than what is offered in the GitLab Inc. policy, then the country-specific policy will apply.

- country list
- country list
- country list

*GitLab’s whistleblower reporting channels and policies should not be used for complaints relating to a team member’s own personal circumstances, or arising out of their employment contract. In those cases you should use the Grievance Procedure or Anti-harassment Policy, or contact Team Member Relations as appropriate. [SR confirm whether we would add hotline vendor details here - or reserve for internal access]

